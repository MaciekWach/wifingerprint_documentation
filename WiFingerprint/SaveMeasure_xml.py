import xml.etree.cElementTree as ET
import ctypes
import datetime

# system language -> supported options
# .---------------------.----------.
# | Languages           | Value    |
# |---------------------|----------|
# | en_US               |   1033   |
# | en_GB               |   2057   |
# | pl_PL               |   1045   |
# '---------------------'----------'

class SaveMeasure_xml():
    """
    Class has methods responsible for save final values to .xml file.
    etree library is used.
    Documentation: https://docs.python.org/2/library/xml.etree.elementtree.html
    """
    @staticmethod
    def search(dictionary, substr):
        """
        Method responsible for search value in dict based on substring
        :param dictionary:
        :param substr:
        :return: searched value
        """
        result = []
        for key in dictionary:
            if substr in key:
                result.append((key, dictionary[key]))
        return result

    @staticmethod
    def save_me(dict, path):
        """
        Main method which is responsible for save final dictionary to .xml file.
        Only information about: SSID, BSSID, Signal Strength and Measure Date are saved.
        :param dict:
        :param path:
        :return: .txt file is created as result - example output at the end on file.
        """
        date = str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M"))
        windows_language_number = ctypes.windll.kernel32.GetUserDefaultUILanguage()
        if windows_language_number in (1033, 2057):
            signal = "Signal"
        elif windows_language_number == 1045:
            signal = "Sygna\x88"
        else:
            print "UNSUPPORTED LANGUAGE"
        root = ET.Element("Measure", date=date)
        for point in dict:
            pix = ET.SubElement(root, "Point")
            ET.SubElement(pix, "Coordinates").text = str(point)
            for SSID in dict[point]:
                ID = ET.SubElement(pix, "SSID", ID=SSID[0], name=SSID[1])
                for BSSID in [k for k in SaveMeasure_xml.search(dict[point][SSID], "BSSID")]:
                    ET.SubElement(ID, "BSSID", ID=BSSID[0]).text = str(BSSID[1])
                ET.SubElement(ID, "SignalStrengthPercentage").text = str(dict[point][SSID][signal])
                ET.SubElement(ID, "SignalStrengthDbm").text = str(dict[point][SSID]['dBm'])
        tree = ET.ElementTree(root)
        tree.write(path + ".xml")

"""
<Measure date="13-08-2017 23:28">
    <Point>
        <Coordinates>(128.0, 273.0)</Coordinates>
        <SSID ID="SSID5" name="UPCWi-Free">
            <BSSID ID="BSSID3">92:5c:14:71:e5:42</BSSID>
            <BSSID ID="BSSID2">c6:27:95:bf:9b:e9</BSSID>
            <BSSID ID="BSSID1">fe:b4:e6:79:c1:a5</BSSID>
            <SignalStrengthPercentage>28%</SignalStrengthPercentage>
            <SignalStrengthDbm>-84</SignalStrengthDbm>
        </SSID>
        <SSID ID="SSID4" name="UPC7286441">
            <BSSID ID="BSSID1">fc:b4:e6:79:c1:a3</BSSID>
            <SignalStrengthPercentage>26%</SignalStrengthPercentage>
            <SignalStrengthDbm>-86</SignalStrengthDbm>
        </SSID>
        <SSID ID="SSID3" name="UPCFA38DA5">
            <BSSID ID="BSSID2">90:5c:44:c7:97:e3</BSSID>
            <BSSID ID="BSSID1">90:5c:44:c7:98:19</BSSID>
            <SignalStrengthPercentage>20%</SignalStrengthPercentage>
            <SignalStrengthDbm>-92</SignalStrengthDbm>
        </SSID>
        <SSID ID="SSID1" name="UPC247963187">
            <BSSID ID="BSSID1">00:36:76:b6:50:fc</BSSID>
            <SignalStrengthPercentage>36%</SignalStrengthPercentage>
            <SignalStrengthDbm>-75</SignalStrengthDbm>
        </SSID>
        <SSID ID="SSID2" name="UPC332070">
            <BSSID ID="BSSID1">e8:40:f2:e5:06:45</BSSID>
            <SignalStrengthPercentage>99%</SignalStrengthPercentage>
            <SignalStrengthDbm>-10</SignalStrengthDbm>
    </Point>
</Measure>
"""