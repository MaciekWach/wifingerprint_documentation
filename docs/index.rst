.. WiFingerprint documentation master file, created by
   sphinx-quickstart on Tue Sep 12 13:56:57 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WiFingerprint's documentation!
=========================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   GetNetworkInfoWindows
   SaveMeasure
   SaveMeasure_xml
   WiFingerprint


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Documentation for the Code
**************************

WiFingerprint 
=============

Base module in application

.. automodule:: WiFingerprint
   :members:

SaveMeasure
===========

Saving to .txt file

.. automodule:: SaveMeasure
   :members: 

SaveMeasure_xml
===============

Saving to .xml file

.. automodule:: SaveMeasure_xml
   :members: 

GetNetworkInfoWindows
=====================

Get all necessary information about visible Wi-Fi networks

.. automodule:: GetNetworkInfoWindows
   :members: 
