import subprocess
import re
import ctypes
import locale
# system language -> supported options
# .---------------------.----------.
# | Languages           | Value    |
# |---------------------|----------|
# | en_US               |   1033   |
# | en_GB               |   2057   |
# | pl_PL               |   1045   |
# '---------------------'----------'
class GetNetworkInfo:
    """
    Class responsible for get and parse information about visible Wi-Fi networks.
    Prepared for Windows OS.
    """
    def __init__(self):
        """
        Constructor which handle required Windows command and convert convention based on Cisco recommendations.
        """
        self.results = subprocess.check_output(["netsh", "wlan", "show", "network", "mode=Bssid"]).replace(" ", "").split("\n\r")[1:-1]
        # a = results.replace(" ", "").split("\n\r")
        # a = a[1:-1]
        self.final_dict = {}
        self.windows_language_number = ctypes.windll.kernel32.GetUserDefaultUILanguage()
        self.convert_dict = {}  # dict[percent] = dbm
        with open("convert_convention.txt") as f:
            for line in f.readlines():
                percent = int(re.search(r'(\d+).*(-\d+)', line).group(1))
                dbm = int(re.search(r'(\d+).*(-\d+)', line).group(2))
                self.convert_dict[percent] = dbm

    def define_os_language(self):
        """
        Method responsible for recognition language of OS and setting names of parameters.
        :return: None
        """
        if self.windows_language_number in (1033, 2057):
            self.signal = "Signal"
            self.radio_type = "Radio type"
            self.channel = "Channel"
        elif self.windows_language_number == 1045:
            self.signal = "Sygna\x88"
            self.radio_type = ""
            self.channel = ""
        else:
            print "UNSUPPORTED LANGUAGE"

    def search(self, dictionary, substr):
        """
        Additional method responsible for search values in dictionary based on substring
        :param dictionary:
        :param substr:
        :return: searched value
        """
        result = []
        for key in dictionary:
            if substr in key:
                result.append((key, dictionary[key]))
        return result

    # Old method to convert percentage quality to dBm - could be useful when file with Cisco convention is unavailable.
    # def quality_to_dbm(self, quality):
    #     if quality <= 0:
    #         dBm = -100
    #     elif quality >= 100:
    #         dBm = -50
    #     else:
    #         dBm = (quality / 2) - 100;
    #     return dBm

    def get_network_info(self):
        """
        Main Method in class responsible for parse output and set properly values to dictionary
        :return: set final values to dict created in constructor.
        """
        self.define_os_language()
        for result in self.results:
            name = re.search(r'(?<=\:).*', ''.join(result).split("\r\n")[0]).group()
            if name == '':
                name = "OtherNetwork"
            ssid = re.search(r'.*?(?=\:)', ''.join(result).split("\r\n")[0]).group()
            self.final_dict[(ssid,name)] = {}
            for value in ''.join(result).split("\r\n"):
                self.final_dict[(ssid, name)][re.search(r'.*?(?=\:)', value).group().strip()] = re.search(r'(?<=\:).*', value).group().strip()
        print self.final_dict
        for result in self.final_dict.keys():
            quality = int(self.final_dict[result][self.signal][:-1])  # final_dict[SSID, name][signal] = 90% [:-1]
            dBm = self.convert_dict[quality]
            self.final_dict[result]["dBm"] = dBm

if __name__ == "__main__":
    # Useful to fast debugging
    import sys
    d = GetNetworkInfo()
    d.get_network_info()
    print d.final_dict

# FINAL DICT HAS STRUCTURE AS BELOW:
'''
dict{(SSID, NAME):
        {value1:key1, value2:key2, ...}
     (SSID2, NAME2):
        {value1:key1, value2:key2, ...}
    }
'''